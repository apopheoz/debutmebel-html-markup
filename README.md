# Вёрстка страниц сайта компании Дебют-Мебель

## Коротко

Этот репозиторий содержит свёрстанные шаблоны страниц сайта компании Дебют-Мебель.

## Демонстрационная вёрстка страниц

**[Главная страница](https://apopheoz.ru/demo/dm/index.html)**

**[О компании](https://apopheoz.ru/demo/dm/about.html)**

**[Дилерам](https://apopheoz.ru/demo/dm/dealers.html)**

**[Каталог](https://apopheoz.ru/demo/dm/catalogue.html)**

**[Категория](https://apopheoz.ru/demo/dm/category.html)**

**[Карточка товара](https://apopheoz.ru/demo/dm/product.html)**

**[Материалы](https://apopheoz.ru/demo/dm/materials.html)**

**[Акционерам](https://apopheoz.ru/demo/dm/actioners.html)**

**[Новости разводящая](https://apopheoz.ru/demo/dm/news.html)**

**[Новости внутренняя](https://apopheoz.ru/demo/dm/news_inside.html)**

**[Статьи разводящая](https://apopheoz.ru/demo/dm/articles.html)**

**[Клиенты](https://apopheoz.ru/demo/dm/clients.html)**

**[Контакты](https://apopheoz.ru/demo/dm/contacts.html)**

**[Фото](https://apopheoz.ru/demo/dm/foto.html)**

## Ссылки

**[apopheoz.ru](https://apopheoz.ru)** - лаборатория веб-дизайна «Апофеозъ»;

## Контактная информация

Отзывы и предложения: https://apopheoz.ru/feedback.html
